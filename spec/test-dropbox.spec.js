const specCfg = require('./spec-config')
const sync = require('../lib/dropbox-sync')
const syncer = sync(specCfg)
const dirlist = require('./fake_dirlist')

async function fakedir(meta) {
  const ret = Object.assign({},dirlist)
  const list = dirlist.entries
  if (meta.path.match(/images/)) {
    ret.entries = list.slice(0, 2)
  }else{
    ret.entries=list.slice(2,4)
  }
  return ret
}

async function fakesend(meta){
  console.log("I have been called")
  return {
    "status": "ok"
  }
}
beforeAll((done) => {
  spyOn(syncer.dbx, "filesDeleteV2").and.callFake(async filespec => { return {status: "ok"}})
  spyOn(syncer.dbx, "filesListFolder").and.callFake(fakedir)
  spyOn(syncer.dbx, "filesListFolderContinue").and.callFake(async spec => [])
  done()
})
describe("DropBox Sync", () => {
  it("synchronizes data", async done => {
    spyOn(syncer.dbx, "filesUpload").and.callFake(fakesend)
    await syncer.sync()
      // expect(syncer.dbx.filesUpload).toHaveBeenCalled()
      //expect(syncer.dbx.filesDeleteV2).toHaveBeenCalled()
      expect(syncer.dbx.filesListFolder).toHaveBeenCalled()
   
      done()
    
   
  })
})
