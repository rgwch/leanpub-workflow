const syncer=require('../lib/compare')

describe("Test Synctool",()=>{
  it("syncs two arrays",(done)=>{
    const source=Array.of("a","b","c","d","e","f","g")
    const dest=Array.of("e","f","g","h","i")
    syncer.twoway(source,dest,(a,b)=>a===b,a=>dest.push(a),b=>dest.splice(dest.indexOf(b),1)).then(res=>{
      expect(source).toEqual(Array.of("a","b","c","d","e","f","g"))
      expect(dest.sort()).toEqual(source)
      done()
    })
  })
})

