module.exports={
  dropbox_api:'should be kept secret',
  leanpub_api: 'dont tell anyone',
  leanpub_slug: 'somecoolbook',
  dropbox_basedir: '/somecoolbook/manuscript',
  local_basedir: './test_struc',
  images_raw: './test_struc/src',
  images_dest: './test_struc/dest',
  images_width: 320
}