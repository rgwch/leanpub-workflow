module.exports={
  "hasMore": false,
  "entries": [
    { '.tag': 'file',
  name: 'large_raw_640x400.jpg',
  path_lower: '/somecoolbook/manuscript/images/large_raw_640x400.jpg',
  path_display: '/somecoolbook/manuscript/images/large_raw_640x400.jpg',
  parent_shared_folder_id: '345058048',
  id: 'id:BE-m4HzbceAAAAAAAACS1s',
  client_modified: '2018-07-03T09:30:31Z',
  server_modified: '2018-07-03T09:30:31Z',
  rev: '328caf52000',
  size: 18012,
  sharing_info:
   { read_only: false,
     parent_shared_folder_id: '345058048',
     modified_by: 'dbid:AAAjoHFVFz5_7lFH54lynFtaf6UBz8_MTVI' },
  content_hash:
   '84baff05bb911a3e7102b13bd861448652d15f8942dfb4a9d1bcf67286fd23b' },
{ '.tag': 'file',
  name: 'small_raw_220x138.jpg',
  path_lower: '/somecoolbook/manuscript/images/small_raw_220x138.jpg',
  path_display: '/somecoolbook/manuscript/images/small_raw_220x138.jpg',
  parent_shared_folder_id: '345058048',
  id: 'id:BE-m4HzbceAAAAAAAACS2B',
  client_modified: '2018-07-03T09:30:32Z',
  server_modified: '2018-07-03T09:30:33Z',
  rev: '329caf52000',
  size: 48287,
  sharing_info:
   { read_only: false,
     parent_shared_folder_id: '340505804',
     modified_by: 'dbid:AAAjoHFVFz5_7lFH54lynFtaf6UBz8_MTVI' },
  content_hash:
   '1038f183c906d28be6dd7c457cb3d18fb75bec83c040a3165d2fa4826db70df' },
{ '.tag': 'file',
  name: 'part1.txt',
  path_lower: '/somecoolbook/manuscript/part1.txt',
  path_display: '/somecoolbook/manuscript/part1.txt',
  parent_shared_folder_id: '340505804',
  id: 'id:BE-m4HzbceAAAAAAAACS2A',
  client_modified: '2018-07-03T09:30:34Z',
  server_modified: '2018-07-03T09:30:34Z',
  rev: '32acaf52000',
  size: 16312,
  sharing_info:
   { read_only: false,
     parent_shared_folder_id: '340505804',
     modified_by: 'dbid:AAAjoHFVFz5_7lF54lynFtaf6UBz8_MTVI' },
  content_hash:
   '0e0f016189f8156e9b2820e416cc40c52c86b9c6b1a6bab040fc458f1d96ba' },
{ '.tag': 'file',
  name: 'part2.txt',
  path_lower: '/somecoolbook/manuscript/part2.txt',
  path_display: '/somecoolbook/manuscript/part2.txt',
  parent_shared_folder_id: '340505804',
  id: 'id:BE-m4HzbceAAAAAAAACS3a',
  client_modified: '2018-07-03T09:30:35Z',
  server_modified: '2018-07-03T09:30:36Z',
  rev: '32bcaf52000',
  size: 14816,
  sharing_info:
   { read_only: false,
     parent_shared_folder_id: '340505804',
     modified_by: 'dbid:AAAjoHFVFz5_7lFH54lynFtaf6UBz8_MTVI' },
  content_hash:
   '1fb069991ef8921ffa14c4c1f1538fd592cd1dadb3d8a8805e3ae79dd69680' }
  ]
}