const _=require('lodash')

async function process(source=[],dest=[],identity=(a,b)=>a===b,destMissing=async (a)=>{},sourceMissing=async (b)=>{},bothExist=async (a)=>{}){

  const left=_.differenceWith(source,dest,identity)
  const right=_.differenceWith(dest,source,identity)
  const both=_.intersectionWith(source,dest,identity)
  
  for(const elem of left){
    await destMissing(elem)
  }
  for(const elem of right){
    await sourceMissing(elem)
  }
  for(const elem of both){
    await bothExist(elem)
  }
  return{
    destMissing: left.length,
    sourceMissing: right.length,
    bothExist: both.length
  }

}

module.exports={
  twoway: process
}
