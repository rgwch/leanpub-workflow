require('isomorphic-fetch');
const flatten = require('array-flatten')
const Dropbox = require('dropbox').Dropbox;
const sync = require('./compare')
const fs = require('fs')
const path = require('path')
const buffers = require('stream-buffers')
const replace = require('replacestream');
const rext = require('replace-ext')

module.exports = function (cfg) {
  const dbx = new Dropbox({ accessToken: cfg.dropbox_api });
  const stats = {
    synced: 0,
    deleted: 0,
    unchanged: 0
  }
  async function doSync() {
    Promise.all([loadRemoteFiles(cfg.dropbox_basedir),
    loadRemoteFiles(cfg.dropbox_basedir + "/images")])
      .then(async dirs => {
        const texts = await syncTexts(dirs[0])
        const images = await syncImages(dirs[1])
        console.log("Dropbox-Sync: "+JSON.stringify(stats,null,2))
      })
  }

  return {
    sync: doSync,
    dbx: dbx,
    fetch: fetchFile
  }

  async function fetchFile(pathname, dest) {
    try {
      const result = await dbx.filesDownload({
        path: pathname
      })
      return new Promise(resolve => {
        const output = fs.createWriteStream(path.join(dest, result.name))
        output.write(result.fileBinary)
        output.on('finish', () => {
          output.close()
          resolve("ok")
        })
      })
    } catch (err) {
      console.log(err)
    }
  }

  function identity(a, b) {
    const src = rext(path.basename(a), "").toLocaleLowerCase()
    const dest = rext(path.basename(b), "").toLocaleLowerCase()
    return src === dest
  }

  async function syncTexts(remoteMetadata) {
    const remoteFiles = remoteMetadata.map(entry => entry.path_lower)
    if(!fs.existsSync(cfg.local_basedir)){
      console.log(`Dropbox-Sync: File directory ${cfg.local_basedir} not found`)
      process.exit(-12)
    }
    const localFiles = fs.readdirSync(cfg.local_basedir)
    return await sync.twoway(localFiles, remoteFiles, identity,
      async file => sendTextFile(path.join(cfg.local_basedir, file)),
      async file => deleteRemoteFile(file),
      async file => checkUpdate(path.join(cfg.local_basedir, file), remoteMetadata, sendTextFile)
    )
  }

  /**
   * Synchronize image files
   * @param {} remoteMetaData 
   */
  async function syncImages(remoteMetaData) {
    const remoteFiles = remoteMetaData.map(entry => entry.path_lower)
    const localFiles = fs.readdirSync(cfg.images_dest)
    return await sync.twoway(localFiles, remoteFiles, identity,
      async file => sendFile(path.join(cfg.images_dest, file), cfg.dropbox_basedir+"/images/"+file),
      async file => deleteRemoteFile(file),
      async file => checkUpdate(path.join(cfg.images_dest, file), remoteMetaData, sendFile))

  }

  async function deleteRemoteFile(filepath) {
    try {
      const ret = await dbx.filesDeleteV2({
        path: filepath
      })
      console.log("Deleted from DropBox " + filepath)
      stats.deleted++
    } catch (err) {
      console.log("Error while deleting from DropBox " + filepath + ": " + err)
    }
  }

  async function checkUpdate(localFilePath, remoteDir, func) {
    const remoteFile = remoteDir.find(elem => rext(elem.name, "") === rext(path.basename(localFilePath), ""))
    if (remoteFile) {
      const remote_ts = new Date(remoteFile.client_modified).getTime()
      const local_ts = new Date(fs.statSync(localFilePath).mtime).getTime()
      if (local_ts > remote_ts) {
        console.log("Updating: " + path.basename(localFilePath))
        return func(localFilePath, remoteFile.path_lower)
      }else{
        stats.unchanged++
      }
    }
  }

  /**
   * Send a single file to DropBox. Overwrite if it exists
   * @param {string} localFile: pathname of local file 
   * @param {string} remoteFile: pathname for remote file 
   */
  async function sendFile(localFile, remoteFile) {
    try {
      const contents = fs.readFileSync(localFile)
      await dbx.filesUpload({
        contents: contents,
        path: remoteFile,
        mode: { ".tag": "overwrite" }
      })
      console.log(`Sent to DropBox: \n${localFile}\nas ${remoteFile}\n` )
      stats.synced++
    } catch (err) {
      console.log("Error sending to DropBox " + localFile + ": " + err.error.error_summary)
    }
  }

  async function loadRemoteFiles(dir) {
    const ret = []
    try {
      let result = await dbx.filesListFolder({
        path: dir,
        recursive: false,
        limit: 30
      })
      ret.push(result.entries)
      while (result.has_more) {
        result = await dbx.filesListFolderContinue({ cursor: result.cursor })
        ret.push(result.entries)
      }
      const flat = flatten(ret)
      return flat.filter(entry => entry[".tag"] === 'file');
    } catch (err) {
      console.log(err)
    }
  }

  function sendTextFile(localFilePath, remoteDir) {
    if (localFilePath.endsWith(".md") && fs.existsSync(localFilePath) && fs.statSync(localFilePath).isFile()) {
      const basename = path.basename(localFilePath, "md")
      output = new buffers.WritableStreamBuffer();
      const rs = fs.createReadStream(localFilePath)
        .pipe(replace(/\[(.+?)\]\{\.index\}/g, (match, m) => {
          return m;
        }))
        .pipe(output);
      return new Promise((resolve, reject) => {
        rs.on('finish', async (err) => {
          if (err) {
            console.error("error: " + err)
          } else {
            try {
              const ress = output.getContentsAsString("utf8")
              const destpath = `${cfg.dropbox_basedir}/${basename}txt`
              const result = await dbx.filesUpload({
                contents: ress,
                path: destpath,
                mode: { ".tag": "overwrite" }
              })
              console.log(`Sent to DropBox: \n${localFilePath}\nas ${destpath}\n` )
              stats.synced++
              resolve(result);
            } catch (err) {
              console.log("Error sending to DropBox " + basename + ": " + err.error.error_summary)
              reject(err)
            }
          }
        })
      })
    }
  }
}