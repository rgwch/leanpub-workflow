const fs = require('fs')
const path = require('path')
const rext = require('replace-ext')
const sharp = require('sharp')
const sync = require('./compare')

module.exports = function (cfg) {
  const stats = {
    created: 0,
    updated: 0,
    deleted: 0
  }
  async function processImages() {
    stats.updated=0;
    if(!fs.existsSync(cfg.images_raw)){
      console.log(`Source directory ${cfg.images_raw} not found`)
      process.exit(-11)
    }
    const rawFiles = fs.readdirSync(cfg.images_raw)
    if(!fs.existsSync(cfg.images_dest)){
      require('child_process').execSync("mkdir -p "+cfg.images_dest)
    }
    const destFiles = fs.readdirSync(cfg.images_dest)
    let result=await sync.twoway(rawFiles, destFiles, identity, copyFile, delFile, updateFile)
    stats.created=result.destMissing
    stats.deleted=result.sourceMissing
    console.log("Image Processor: "+JSON.stringify(stats,null,2))
    return stats
  }
  return {
    run: processImages
  }
  function destFile(fname) {
    return path.join(cfg.images_dest, rext(fname, ".jpg"))
  }

  function srcFile(fname) {
    return path.join(cfg.images_raw, fname)
  }
  function identity(a, b) {
    const src = rext(path.basename(a), "")
    const dest = rext(path.basename(b), "")
    return src === dest
  }

  async function copyFile(fname) {
    await doCopy(srcFile(fname), destFile(fname))
  }

  function delFile(fname) {
    fs.unlinkSync(destFile(fname))
  }

  async function updateFile(fname){
    const src=srcFile(fname)
    const dest=destFile(fname)
    const statSrc=fs.statSync(src)
    const statDest=fs.statSync(dest)
    if(statSrc.mtime>statDest.mtime){
      await doCopy(src,dest)
      stats.updated++
      return true
    }else{
      return false
    }
  }

  async function doCopy(file, destfile) {
    try {
      const image = sharp(file)
      const meta = await image.metadata()
      if (meta.width > cfg.images_width) {
        await image.resize(cfg.images_width,null)
        await image.sharpen()
      }
      await image.toFile(destfile)
      console.log("Image procecced: "+destfile)

    } catch (err) {
      console.log(`Error while processing ${file}: ${err}`)
    }
  }

}


