const request = require('request')
const fs = require('fs')

module.exports = function (cfg) {
  const get_uri = func => `https://leanpub.com/${cfg.leanpub_slug}/${func}?api_key=${cfg.leanpub_api}`

  return {
    single: preview_single,
    subset: preview_subset,
    full: preview_full,
    status: job_status
  }

  function job_status(){
    request(get_uri("job_status"), (err,status,body)=>{
      if(err){
        console.log("*** "+err)
      }else if(status.statusCode!=200){
        console.log("Error :"+status.statusMessage+" "+body)
      }else{
        try{
          const result=JSON.parse(body)
          if(result.status){
            console.log(`${result.status} - ${result.num}: ${result.message}`)
          }else{
            console.log("No pending jobs "+body)
          }
        }catch(err){
          console.log("*** unparseable "+body)
        }
      }
    })
  }
  function preview_single(file) {
    try {
      if (file) {
        contents = fs.readFileSync(file)
        send("preview/single.json", contents)
      } else {
        throw ("usage: leanpub-process single <filename>")
      }
    } catch (err) {
      console.log("could not read " + file + ", " + err)
    }
  }

  function preview_subset(){
    send("preview/subset.json")
  }

  function preview_full(){
    send("preview.json");
  }

  function send(func, contents) {
    const options = {
      uri: get_uri(func),
      method: "POST"
    }
    if (contents) {
      options.headers = { "content-type": "text/plain" }
      options.body = contents
    }
    request(options, (error, response, body) => {
      if (error) {
        console.log("*** "+error)
      } else {
        if (response.statusCode == 200) {
          console.log(body)
        } else {
          console.log("*** Error: " + body)
        }
      }
    })
  }
}



