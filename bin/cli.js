#! /usr/bin/env node

const menu = require('console-menu')
const path = require('path')
const fs = require('fs')

const optionDefinitions = {
  alias: {
    config: "c"
  }

}
const cmds = require('minimist')(process.argv.splice(2),optionDefinitions)
let cfg;

try {
  cfg = JSON.parse(fs.readFileSync(cmds.config || "./leanpub-config.json"))
} catch (err) {
  console.log("error while trying to load config file: " + err)
  process.exit(-10)
}

const imageProcessor = require('../lib/image-process')(cfg)
const syncer = require("../lib/dropbox-sync")(cfg)
const leanpub = require('../lib/leanpub')(cfg)

if(cmds.v){
  showVersion()
  process.exit(0)
}

if (cmds._ && cmds._.length > 0) {
  handleArgs(cmds._)
} else {
  showMenu()
}

function showMenu() {
  menu([
    { hotkey: "1", title: "Process images (=images)" },
    { hotkey: "2", title: "Sync Images and texts (=sync)" },
    { separator: true },
    { hotkey: "3", title: "Create single file preview (=single)" },
    { hotkey: "4", title: "Create subset preview (=subset)" },
    { hotkey: "5", title: "Create full preview (=full)" },
    { separator: true, title: "--------------" },
    { hotkey: "6", title: "Show job status (=status)" },
    { hotkey: "7", title: "Download preview (=fetch)" },
    { separator: true },
    { hotkey: "v", title: "Show version (=version)" },
    { hotkey: "x", title: "Exit" }
  ], {
      header: "Leanpub workflow options",
      border: true
    }).then(async item => {
      console.log("\033[2J")
      if (item) {
        switch (item.hotkey) {
          case "1": imageProcessor.run(); break;
          case "2": syncer.sync(); break;
          case "3": leanpub.single(args[1]); break;
          case "4": leanpub.subset(); break;
          case "5": leanpub.full(); break;
          case "6": leanpub.status(); break;
          case "7": syncer.fetch(`/${cfg.leanpub_slug}/preview/${cfg.leanpub_slug}-preview.pdf`, "."); break;
          case "v": showVersion(); break;
          case "x": process.exit(0)

        }
      }
      // showMenu()
    })
}


function handleArgs(exec) {
  if (exec && Array.isArray(exec) && exec.length > 0) {
    switch (exec[0]) {
      case 'images': imageProcessor.run(); break;
      case 'sync': syncer.sync(); break;
      case 'single': leanpub.single(exec[1]); break;
      case 'subset': leanpub.subset(); break;
      case 'full': leanpub.full(); break;
      case 'status': leanpub.status(); break;

      case 'version': showVersion(); break;
      case 'fetch': syncer.fetch(`/${cfg.leanpub_slug}/preview/${cfg.leanpub_slug}-preview.pdf`, "."); break;
    }
  }
}

function showVersion() {
  let package = JSON.parse(fs.readFileSync(path.join(__dirname, '../package.json')))
  console.log("leanpub-workflow v" + package.version)
}