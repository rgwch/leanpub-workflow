## About

This utility helps with creating and publishing an e-book via [Leanpub](https://leanpub.com). It covers processing of images, syncing of images and text files with DropBox, and creating full or partial previews on Leanpub.

## Install

npm install -g leanpub-workflow

## Preparation

You'll need a Leanpub account and a Dropbox account. There's no need for a DropBox client on the PC (but it doesn't harm either).

* Select the "dropbox" type for your Leanpub project.

* Create a new App in your Dropbox-Account (<https://www.dropbox.com/developers/apps?_tk=pilot_lp&_ad=topbar4&_camp=myapps>) and generate an access-token for that app. 

* Go to your Leanpub account <https://leanpub.com/author_dashboard/settings> and find your API-Key.

* create a leanpub-config.json file as:

~~~~~~~~~
{  
  "dropbox_api":"dont-tell-anybody",
  "leanpub_api": "keep-it-secret",
  "leanpub_slug": "mycoolbook",
  "dropbox_basedir": "/mycoolbook/manuscript",
  "local_basedir": "/home/myname/bookproject",
  "images_raw": "/home/myname/bookproject/img-raw",
  "images_dest": "/home/myname/bookproject/images",
  "images_width": 480
}
~~~~~~~~~


## Usage

* interactive mode: When called without parameters, leanpub-workflow will present a menu to chose from.

* batch mode: When called with one or more parameters, leanpub will not show the menu, but instead execute the command given in the first parameter.

### Configuration

By default, leanpub-workflow tries to load a file 'leanpub-config.json' from the current directory. You can chose a different file with the option --config or -c:

`leanpub-workflow -c </path/to/other-config.json> [command]` 

Possible operations are:

### Option 1: Process images - command: images

This will consider all images found in the folder denoted by *images_raw* in the config and copy them to the folder *images_dest*.

* If the image width is above the *images_width* config, it will resize the image so that the width is images_width and the height is reduced accordingly to keep aspect ratio.

* If the image width is less than *images_width*, does not change size.

* If the Image is not in jpg-Format, converts to jpg

* Original images are not modified. 

### Option 2: Sync images and texts - command: sync

This will consider all images in *images_dest* and all *.md files in *local_basedir*

* If a file exists locally but not in the Dropbox -> send it to DropBox.

* If a file exists in the DropBox, but not locally -> delete it in the DropBox.

* If a file is newer locally than in the DropBox -> Send it to the DropBox.

* If it is a file with *.md Extension, send it with a *.txt extension.

* Files which are not in the images_dest folder and have not the extension .md are ignored.

### Option 3: Create single file preview - command: single

Doesn't work. Currently gets always a "500" error from the server. Probably I didn't get the syntax right.

### Option 4: Create subset preview - command: subset

Create a Preview from the files named in Subset.md/Subset.txt. The Preview is stored in the Dropbox previews-Folder.

### Option 5: Create full preview - command: full

Create a Preview from the files named in Book.md/Book.txt. The preview will be stored in the Dropbox previews-Folder.

### Option 6: Show job status - command: status

Leanpub jobs can take quite some time. The status command shows if a job is still running, and what is its state.

### Option 7: Download preview - command: fetch

Downloads the last preview pdf from the Dropbox to the current folder. The Filename will be ${leanpub_slug}-preview.pdf

### Option v: Show Version - command: version

Display the program version and exit

## Tests

npm test

## License

Apache 2.0